#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <inttypes.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "Libs/HD44780/hd44780.h"

#include <string.h>
#include <stdio.h>
#include <time.h>

#define BUAD 9600
#define BUAD_RATE_CALC ((F_CPU/16/BUAD) - 1)

volatile uint8_t _isUp;

ISR(INT0_vect)
{
	_isUp = 1;
}

void serialInit(void){
    //Register settings
    //High and low bits
    UBRR0H = (BUAD_RATE_CALC >> 8);
    UBRR0L = BUAD_RATE_CALC;
    //transimit and recieve enable
    UCSR0B = (1 << TXEN0)| (1 << TXCIE0) | (1 << RXEN0) | (1 << RXCIE0);
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);  //8 bit data format
}

void serialSend(const char* const sendString){
     for (unsigned int i = 0; i < strlen(sendString); i++){
         while (( UCSR0A & (1<<UDRE0))  == 0){
        	 // waiting
         };
         UDR0 = sendString[i];
     }
}

void adcInit(void)
{
	//ADC
		  ADMUX = (0<<REFS1)|(1<<REFS0)|(1<<ADLAR);		// AVcc
		  ADMUX |= 0;									// Select ADC Channel 0 Initially
		  ADCSRA = (1<<ADEN)|(0<<ADSC)|(0<<ADATE)|(0<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
		  ADCSRB = (0<<ADTS2)|(0<<ADTS1)|(0<<ADTS0);
		  DIDR0 = (1<<ADC5D)|(1<<ADC4D)|(1<<ADC3D)|(1<<ADC2D)|(1<<ADC1D)|(0<<ADC0D);
}

int main(void)
{
	  lcd_init();
	  lcd_clrscr();
	  lcd_puts("F_CPU=");
	  const int kFcpuValueStringLength = 9;
	  char fcpuString[kFcpuValueStringLength];
	  snprintf(fcpuString, kFcpuValueStringLength, "%li", F_CPU);
	  lcd_puts(fcpuString);
	  serialSend(fcpuString);

	  adcInit();
	  serialInit();

//	GIMSK = 1<<INT0;				// Enable INT0
//	MCUCR = 1<<ISC01;				// Trigger INT0 on falling edge
//	MCUCR &= ~(1<<ISC00);

//	sei();

//	DDRB = 0xFF;
    DDRB |= 1<<PB5; /* set PB0 to output */  //0b00000001
//    DDRB |= 1<<PB1;
	PORTB =  0xFF;
    while(1)
    {
    	lcd_goto(0x40);
    	lcd_puts("            ");

//       _delay_ms(500);
       PORTB |= 1<<PB5; /* LED off */
//       PORTB = 0x00;
//       PORTB = 0b11111111;
       _delay_ms(100);
       PORTB &= ~(1<<PB5); /* LED on */
//       PORTB =  0xFF;
//       PORTB = 0b00000000;



//        uint8_t isUp;
//        ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
//        {
//        	isUp = _isUp;
//        	_isUp = 0;
//        }
//
//        if (isUp)
//        {
//        	PORTB &= ~(1<<PB1); /* LED on */
//        }
//        else
//        {
//        	PORTB |= 1<<PB1;
//        }

       lcd_goto(0x40);
//       lcd_putc('1');

//       clock_t startClock = clock();
       // Start Conversion
           ADCSRA |= (1<<ADSC);



           // Wait for Conversion to finish
           while(ADCSRA & (1 << ADSC));

//           clock_t endClock = clock();
//           char diffClockString[kFcpuValueStringLength];
//           snprintf(diffClockString, 9, "%li", (endClock-startClock));
//           lcd_puts(diffClockString);

           lcd_putc('-');

           // Return ADC Value

           const int kAdcValueStringLength = 4;
           char x_str[kAdcValueStringLength];
           snprintf(x_str, kAdcValueStringLength, "%d", ADCH);
           lcd_puts(x_str);
           serialSend(x_str);

           _delay_ms(100);
    }
    return 0;
}
